/**
 * Задача 2.
 *
 * Дописать требуемый код что бы код работал правильно.
 * Необходимо присвоить переменной result значение true, 
 * если строка source содержит подстроку spam. Иначе — false.
 *
 * Условия:
 * - Необходимо выполнить проверку что source и spam являются типом string.
 * - Строки должны быть не чувствительными к регистру
 */

 function truncate(string, maxLength) {
    // РЕШЕНИЕ НАЧАЛО
    if (typeof string === 'string' && typeof maxLength === 'number' && string.length > maxLength) {
        const truncatedString = string.slice(0, maxLength - 3);
        
       // return truncatedString + '...';
       return `${truncatedString}...`;
    } else {
        return  string;
    }
    // РЕШЕНИЕ КОНЕЦ
}

console.log(truncate('Вот, что мне хотелось бы сказать на эту тему:', 21)); // 'Вот, что мне хотел...'
